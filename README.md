# Bienvenidos a SYSTEM X!

## La empresa de consolas más segura del mundo

![](https://gitlab.com/terceranexus6/iot_project_salesianos/-/raw/master/images/SYSTEMX.gif)

Este es el repositorio de SYSTEMX, somos una empresa de diseño de consolas superseguras. Estamos trabajando en revolucionarios sistemas de entretenimiento, aunque seamos un pequeño equipo de personas.
